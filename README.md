# Description
This repository contains pipeline files to test linux kernels.

# Dependencies
* [common](https://gitlab.com/mrchapp/common)

These files need to be included into the pipeline config file you choose from
the `gitlab-ci` directory.
For the queue pipeline the files are located in `queues`.

This is done in the `bootstrap.yml` file.

# Usage
The goal is to only change the files under the `root` directory based on how LKFT want to test a linux kernel.
Testing the `queues` repository was different so that goes into its own directory.

## Variables
Variables which require user-specific values should be set in the `bootstrap.yml` file.

Variables which require linux kernel specific values should be set in the appropriate file in the `gitlab-ci` directory.
